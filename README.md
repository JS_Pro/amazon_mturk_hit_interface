# README #

This is Amazon MTurk HIT Interface repository.

### Issues

1. To show web image in Chrome, You should install "Allow-Control-Allow-Origin" Extension
2. To show web image in Firefox, You should install "CORS Everywhere" Extension
3. When add button in html code, You should insert 'type = "button"' exactly.
   If omit this, MTurk makes error : "There was a problem submitting your results for this HIT. ..."
  